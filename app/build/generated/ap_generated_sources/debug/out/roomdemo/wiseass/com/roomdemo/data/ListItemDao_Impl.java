package roomdemo.wiseass.com.roomdemo.data;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Exception;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@SuppressWarnings({"unchecked", "deprecation"})
public final class ListItemDao_Impl implements ListItemDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<ListItem> __insertionAdapterOfListItem;

  private final EntityDeletionOrUpdateAdapter<ListItem> __deletionAdapterOfListItem;

  public ListItemDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfListItem = new EntityInsertionAdapter<ListItem>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `ListItem` (`itemId`,`message`,`colorResource`) VALUES (?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ListItem value) {
        if (value.getItemId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getItemId());
        }
        if (value.getMessage() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getMessage());
        }
        stmt.bindLong(3, value.getColorResource());
      }
    };
    this.__deletionAdapterOfListItem = new EntityDeletionOrUpdateAdapter<ListItem>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `ListItem` WHERE `itemId` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ListItem value) {
        if (value.getItemId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getItemId());
        }
      }
    };
  }

  @Override
  public Long insertListItem(final ListItem listItem) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfListItem.insertAndReturnId(listItem);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteListItem(final ListItem listItem) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfListItem.handle(listItem);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public LiveData<ListItem> getListItemById(final String itemId) {
    final String _sql = "SELECT * FROM ListItem WHERE itemId = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (itemId == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, itemId);
    }
    return __db.getInvalidationTracker().createLiveData(new String[]{"ListItem"}, false, new Callable<ListItem>() {
      @Override
      public ListItem call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfItemId = CursorUtil.getColumnIndexOrThrow(_cursor, "itemId");
          final int _cursorIndexOfMessage = CursorUtil.getColumnIndexOrThrow(_cursor, "message");
          final int _cursorIndexOfColorResource = CursorUtil.getColumnIndexOrThrow(_cursor, "colorResource");
          final ListItem _result;
          if(_cursor.moveToFirst()) {
            final String _tmpItemId;
            _tmpItemId = _cursor.getString(_cursorIndexOfItemId);
            final String _tmpMessage;
            _tmpMessage = _cursor.getString(_cursorIndexOfMessage);
            final int _tmpColorResource;
            _tmpColorResource = _cursor.getInt(_cursorIndexOfColorResource);
            _result = new ListItem(_tmpItemId,_tmpMessage,_tmpColorResource);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public LiveData<List<ListItem>> getListItems() {
    final String _sql = "SELECT * FROM ListItem";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"ListItem"}, false, new Callable<List<ListItem>>() {
      @Override
      public List<ListItem> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfItemId = CursorUtil.getColumnIndexOrThrow(_cursor, "itemId");
          final int _cursorIndexOfMessage = CursorUtil.getColumnIndexOrThrow(_cursor, "message");
          final int _cursorIndexOfColorResource = CursorUtil.getColumnIndexOrThrow(_cursor, "colorResource");
          final List<ListItem> _result = new ArrayList<ListItem>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final ListItem _item;
            final String _tmpItemId;
            _tmpItemId = _cursor.getString(_cursorIndexOfItemId);
            final String _tmpMessage;
            _tmpMessage = _cursor.getString(_cursorIndexOfMessage);
            final int _tmpColorResource;
            _tmpColorResource = _cursor.getInt(_cursorIndexOfColorResource);
            _item = new ListItem(_tmpItemId,_tmpMessage,_tmpColorResource);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }
}
